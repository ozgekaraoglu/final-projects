#include"Count.h"
#include<iostream>
using namespace std;

void setX(Count &c, int val){
	c.x = val;
}
int main(){
	
	Count counter;
	cout << "counter.x after instantiation: ";
	counter.print();
	
	setX(counter,8);
	cout << "counter.x after call to setX friend function: ";
	counter.print();

	return 0;
}