/* Design a class to store the information of students in a university. * What kind of information do we need to store about a university student?
 * What kind of functions can be implemented inside the student class?
*/

#include<iostream>
#include<string>
using namespace std;

class Student{

public:

	Student(string name,string surname,int studentID);
	bool studentGraduated();

private:

	string name,surname;
	int studentID;
	double GPA;
	int creditCompleted;
};

int main(){


}