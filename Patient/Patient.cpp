/* Design a class to store the information of patients for a hospital management system
 * What kind of information do we need to store about a patient? (properties)
 * What kind of functions can be implemented inside the patient class?
*/
#include<string>
#include<iostream>
using namespace std;

class Patient{

public:

	Patient(string fname,string surname,string ssn);
	Patient(string fname,string surname,string dateOfBirth,string ssn);
	double BMI();

private:

	string fname, surname, dateOfBirth, ssn;
	double height,weight;
};

int main()

{


return 0;
}