// FoodMenu.cpp : Defines the entry point for the console application.
//

#include "Menu.h"
#include<iostream>

double Food::taxRate = 0.15;

int main()
{
	Menu m1;
	
	m1.print();

	for(int i = 0; i < m1.get_n_foods(); i++)
		cout << m1.getFood()[i].getTaxRate() << endl;

	return 0;
}

