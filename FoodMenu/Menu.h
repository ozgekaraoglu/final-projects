#include "Food.h"

class Menu {
private:
	Food *foods;	
	int n_foods;

public:
	Menu();
	~Menu();

	int get_n_foods() const;
	void set_n_foods(int );

	Food* getFood() const;

	void print() const;
};