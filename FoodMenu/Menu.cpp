#include "Menu.h"
#include<iostream>

using namespace std;

Menu::Menu()
{
	int s;
	cout << "Enter the number of foods: ";
	cin >> s;

	set_n_foods(s);
	foods = new Food[n_foods];
}

Menu::~Menu()
{
	delete [] foods;
	cout << "Menu object destroyed" << endl;
	set_n_foods(0);
	cout << "---------------------" << endl;

}

void Menu::print() const
{
	for(int i = 0; i < n_foods; i++)
	{
		cout << "--------------------" << endl;
		cout << foods[i].getName() << endl;
		cout << foods[i].getPrice() << endl;
		cout << foods[i].getCalory() << endl;
		cout << "--------------------" << endl;
	}
}

int Menu::get_n_foods() const
{
	return n_foods;
}

void Menu::set_n_foods(int n)
{
	while(n < 0)
	{
		cout << "Enter number of foods: ";
		cin >> n;
	}

	n_foods = n;
}

Food* Menu::getFood() const
{
	return foods;
}