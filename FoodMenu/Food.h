#include<string>

using namespace std;

class Food {
private:
	string name;
	double price, calory;

	static double taxRate;
public:
	Food();
	Food(string , double, double);

	void setName(string );
	string getName() const;

	void setPrice(double );
	double getPrice() const;
	
	void setCalory(double );
	double getCalory() const;

	static double getTaxRate();
};