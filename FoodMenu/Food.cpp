#include "Food.h"
#include<iostream>

using namespace std;

Food::Food()
{
	string n;
	double p, c;
	cout << "Enter name of food: ";
	cin >> n;
	setName(n);
	
	cout << "Enter price of food: ";
	cin >> p;
	setPrice(p);
	
	cout << "Enter calory of food: ";
	cin >> c;
	setCalory(c);
}

void Food::setName(string n)
{
	name = n;
}

string Food::getName() const
{
	return name;
}

void Food::setPrice(double p)
{
	price = p > 0 ? p : 0;
}

double Food::getPrice() const
{
	return price;
}

void Food::setCalory(double c)
{
	calory = c > 0 ? c : 0;
}

double Food::getCalory() const
{
	return calory;
}

double Food::getTaxRate()
{
	return taxRate;
}
