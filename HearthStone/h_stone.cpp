// h_stone.cpp : Defines the entry point for the console application.
//

#include "Card.h"
#include "Monster.h"
#include "Spell.h"
#include<iostream>

using namespace std;

int Card::n_cards = 0;

int main()
{
	Card* c;
	
	Monster m2(10, 20, 200);
	Monster *m = new Monster(5, 25, 100);
	c = m;

	c->print();

	m->attack(&m2);

	delete m;
	return 0;
}

