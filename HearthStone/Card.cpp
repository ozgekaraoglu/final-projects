#include<iostream>
#include "Card.h"

using namespace std;

Card::Card()
{
	int c, d;

	cout << "Enter cost: ";
	cin >> c;
	cout << "Enter damage: ";
	cin >> d;
	
	setCost(c);
	setDamage(d);
	
	n_cards++;
}

Card::Card(int c, int d)
{
	setCost(c);
	setDamage(d);
	n_cards++;
}

Card::~Card()
{
	cout << "Character died!" << endl;
	n_cards--;
}

void Card::setCost(int c)
{
	while(c <= 0)
	{
		cout << "Enter a positive cost: ";
		cin >> c;
	}

	cost = c;
}

int Card::getCost() const
{
	return cost;
}

void Card::setDamage(int d)
{
	while(d <= 0)
	{
		cout << "Enter a positive damage: ";
		cin >> d;
	}

	damage = d;
}

int Card::getDamage() const
{
	return damage;
}

void Card::print() const
{
	cout << "Cost: " << getCost() << endl;
	cout << "Damage: " << getDamage() << endl;
}


