#ifndef SPELL_H
#define SPELL_H

#include "Card.h"
#include "Monster.h"

class Spell : public Card {
private:
	int healing;

public:
	Spell();
	Spell(int , int , int);

	void setHealing(int );
	int getHealing() const;

	void print() const;

	virtual void attack(Monster* );
};

#endif