#ifndef MONSTER_H
#define MONSTER_H

#include "Card.h"

class Monster : public Card {
private:
	int health;

public:
	Monster();
	Monster(int , int , int );

	void setHealth(int );
	int getHealth() const;

	void print() const;

	virtual void attack(Monster* );
};

#endif