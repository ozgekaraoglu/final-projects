#include "Monster.h"
#include<iostream>

using namespace std;

Monster::Monster()
{
	int h;
	cout << "Enter health: ";
	cin >> h;
	setHealth(h);
}

Monster::Monster(int c, int d, int h)
	: Card(c, d)
{
	setHealth(h);
}

void Monster::setHealth(int h)
{
	while(h <= 0)
	{
		cout << "Enter a positive health: ";
		cin >> h;
	}

	health = h;
}

int Monster::getHealth() const
{
	return health;
}

void Monster::print() const
{
	Card::print();
	cout << "Health: " << getHealth() << endl;
}

void Monster::attack(Monster* m)
{
	m->setHealth(m->getHealth() - this->getDamage());
	
	if(m->getHealth() <= 0)
		delete m;
}
