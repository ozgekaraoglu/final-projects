#ifndef CARD_H
#define CARD_H

class Card {
protected:
	int cost, damage;
	static int n_cards;

public:
	Card();
	Card(int , int );

	~Card();

	void setCost(int );
	int getCost() const;

	void setDamage(int );
	int getDamage() const;

	virtual void print() const;
};

#endif
