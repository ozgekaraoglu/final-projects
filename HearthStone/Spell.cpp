#include "Card.h"
#include<iostream>
#include "Spell.h"

using namespace std;

Spell::Spell()
{
	int h;
	cout << "Enter healing power: ";
	cin >> h;
	setHealing(h);
}

Spell::Spell(int c, int d, int h)
	: Card(c, d)
{
	setHealing(h);
}

void Spell::setHealing(int h)
{
	while(h <= 0)
	{
		cout << "Enter healing power: ";
		cin >> h;
	}

	healing = h;
}

int Spell::getHealing() const
{
	return healing;
}

void Spell::print() const
{
	Card::print();
	cout << "Healing power: " << getHealing() << endl;
}

void Spell::attack(Monster* m)
{
	m->setHealth(m->getHealth() - 2 * this->getDamage());
	
	if(m->getHealth() <= 0)
		delete m;
}
