#pragma once
#include<iostream>
using namespace std;
class Increment
{
public:
	//default constructor
	Increment(int c = 0, int i = 1);
	//function addIncrement definition
	void addIncrement(){
		count += increment;
	}

	//prints count and increment
	void print() const;
	~Increment(void);

private:
	int count;
	//const data member
	const int increment;
};

